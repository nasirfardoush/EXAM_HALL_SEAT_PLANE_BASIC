<!DOCTYPE html>
<html lang="en">
<head>
  <title>Exam Hall Seat Plan |DIU</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" type="text/css" href="{{ asset('/css') }}/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css') }}/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">DIU</a>
	    </div>
	    <ul class="nav navbar-nav">
	      <li ><a href="#">Home</a></li>
	      <li class="active">
	      	<a   href="#">SEAT PLAN </a>
	      </li>
	    </ul>
	    <ul class="nav navbar-nav navbar-right">
	      <li><a  href="/register"> Sign Up</a></li>
	      <li><a  href="/login">Login</a></li>
	    </ul>
	  </div>
	</nav>
	<div class="container">
	   <div class="row">
	   		<div class="maincontent">
	   			<div class="main_heading">
	   				<a class="download btn btn-primary" href="">Dowanload</a>
	   		@forelse($examData as $exdata)		
	   				<p>{{ $exdata->title }}</p>
						<div class="main_heading_middle_content">
							<span class="pull-left">Date:  {{$exdata->date}} </span>
	   						<span class="pull-right"> Slot: {{$exdata->slot}} </span>
						</div>
	   				<p>Total Seat: {{$exdata->total_seat}} </p>
	   				<p class="boder_top">MASTER SEAT PLAN</p>
	   			</div>
	   			<div class="content_body">
	@empty
	@endforelse
					{{-- Content Section --}}
				@php
					$colno = 0;
				@endphp
				@forelse($seatPlanData as $data)
					@php
						$colno++;
					@endphp
	   				<section class="row">
	   					<h4><u>CSE Building</u></h4>

	   					<h4>{{ $data->room }} </h4>
		   				<table border="1" cellpadding="0" cellspacing="0">							
								
							<th>
								@if(!empty($data->col1_text))
									COL-1
								@endif
							</th>								
							<th>
								@if(!empty($data->col2_text))
									COL-2
								@endif
							</th>								
							<th>
								@if(!empty($data->col3_text))
									COL-3
								@endif
							</th>								
							<th>
								@if(!empty($data->col4_text))
									COL-4
								@endif
							</th>								
							<th>
								@if(!empty($data->col5_text))
									COL-5
								@endif
							</th>							
							<th>
								@if(!empty($data->col6_text))
									COL-6
								@endif
							</th>							
							<th>
								@if(!empty($data->col7_text))
									COL-7
								@endif
							</th>							
							<th>
								@if(!empty($data->col8_text))
									COL-8
								@endif
							</th>							
							<th>
								@if(!empty($data->col9_text))
									COL-9
								@endif
							</th>													
							<th>
								@if(!empty($data->col10_text))
									COL-10
								@endif
							</th>						
							<tbody>
								<tr>
									<td>
										{{ $data->col1_text }}
									</td>									
									<td>
										{{ $data->col2_text }}
									</td>									
									<td>
										{{ $data->col3_text }}
									</td>									
									<td>
										{{ $data->col4_text }}
									</td>									
									<td>
										{{ $data->col5_text }}
									</td>									
									<td>
										{{ $data->col6_text }}
									</td>									
									<td>
										{{ $data->col7_text }}
									</td>									
									<td>
										{{ $data->col8_text }}
									</td>									
									<td>
										{{ $data->col9_text }}
									</td>									
									<td>
										{{ $data->col10_text }}
									</td>								
								</tr>							
								<tr>
									<td>
										{{ $data->col1_seat }}
									</td>									
									<td>
										{{ $data->col2_seat }}
									</td>									
									<td>
										{{ $data->col3_seat }}
									</td>									
									<td>
										{{ $data->col4_seat }}
									</td>									
									<td>
										{{ $data->col5_seat }}
									</td>									
									<td>
										{{ $data->col6_seat }}
									</td>									
									<td>
										{{ $data->col7_seat }}
									</td>									
									<td>
										{{ $data->col8_seat }}
									</td>									
									<td>
										{{ $data->col9_seat }}
									</td>									
									<td>
										{{ $data->col10_seat }}
									</td>
								</tr>
							</tbody>
						</table>
	   				</section>
	   			@empty
	   				<h3>No data Found!!</h3>
	   			@endforelse		

	   			</div>
	   		</div>
	   </div>
	  </div>
	<div class="footer">
		<p>Alright reserved by:</p>
	</div>
</body>
</html>
