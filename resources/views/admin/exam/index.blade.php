@extends('admin.layouts.master')
@section('educations_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">Hall Seat Plan</span>  || <a href="/exam/create"> ADD NEW EXAM INFORMATION.</a>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
	 <div class="table-responsive">
		<table class="table bg-slate-600">
			<thead>
				<tr>
					<th colspan="7"><h2 class="text-center">
							@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
							@else
								<h4>Exam Information</h4>	
							@endif
					</h2></th>
				</tr>				
				<tr>
					<th>Sl no</th>
					<th>Title</th>
					<th>Date</th>
					<th>Slot</th>
					<th>Total seat</th>
					<th colspan="2">Manage</th>
				</tr>
			</thead>
			<tbody>
			@php $sl=0; @endphp
			@forelse($examdata as $data)
				@php $sl++; @endphp
				<tr>
					<td>{{ $sl }}</td>
					<td>{{ $data->title }}</td>
					<td>{{ $data->date }}</td>
					<td>{{ $data->slot }}</td>
					<td>{{ $data->total_seat }}</td>
					<td>
						<a class="btn-success" href="/exam/{{ $data->id }}/edit">Edit</a> 
					</td>									
					<td>
						{{ Form::open(['url'=>['/exam',$data->id],'method'=>'DELETE']) }}
								{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
						{{ Form::close() }}
					</td>
				</tr>
			@empty
			<tr>
				<td colspan="7">
					<h5>No Data found</h5>
				</td>	
			</tr>
			@endforelse

													
			</tbody>
		</table>
	 </div>
 </div>
</div> 	
@endsection