@extends('admin.layouts.master')
@section('educations_menu_add','active')
@section('pageTitle')
<span class="text-semibold">EDUCATIONS - ADD</span>  || <a href="/educations">MY EDUCATIONS</a>
@endsection

@section('content')
	<div class="row ">
		{!! Form::open(['url'=>['/exam',$data->id],'method'=>'PUT']) !!}
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
						<div class="row">
						@if(Session::has('message'))
								<div class="alert alert-info " >
									{{ Session::get('message') }}
								</div>
							@else
								<h5>Add your educations .</h5>
							@endif
							<!-- section one -->
							<div class="col-md-5">
								<div class="form-group">
									
									{!! Form::label('title','Exam Title') !!}
									{!! Form::text('title',$data->title,['placeholder'=>'Summer-2016','class'=>'form-control']) !!}
								</div>					
								<div class="form-group">
								    {!! Form::label('date','Exam start date') !!}
									{!! Form::text('date',$data->date,['placeholder'=>'2013,03','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
								    {!! Form::label('slot','Slot') !!}
									{!! Form::text('slot',$data->slot,['class'=>'form-control']) !!}
								</div>

								<div class="form-group">
								    {!! Form::label('total_seat','Total seat') !!}
									{!! Form::text('total_seat',$data->total_seat,['placeholder'=>'2011','class'=>'form-control']) !!}
								</div>
							</div>
						</div>
						<div class="form-group">
						{!! Form::submit('Save',['class'=>'marg-top']) !!}
							
						</div>
					</div>
				</div>
			</fieldset>
		{!! Form::close() !!}
   	</div>
@endsection