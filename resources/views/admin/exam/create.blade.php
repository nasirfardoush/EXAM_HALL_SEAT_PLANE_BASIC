@extends('admin.layouts.master')
@section('exam_menu_add','active')
@section('pageTitle')
<span class="text-semibold">EXAM INFORMATION - ADD</span>  || <a href="/exam">EXAM INFORMATION </a>
@endsection

@section('content')
	<div class="row ">
		{!! Form::open(['url'=>'/exam','method'=>'POST']) !!}
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
						<div class="row">
						@if(Session::has('message'))
								<div class="alert alert-info " >
									{{ Session::get('message') }}
								</div>
							@else
								<h5>Add Exam details .</h5>
							@endif
							<!-- section one -->
							<div class="col-md-5">
								<div class="form-group">
									
									{!! Form::label('title','Exam Title') !!}
									{!! Form::text('title',null,['placeholder'=>'Summer-2016','class'=>'form-control']) !!}
								</div>					
								<div class="form-group">
								    {!! Form::label('date','Exam start date') !!}
									{!! Form::text('date',null,['placeholder'=>'2013,03','class'=>'form-control']) !!}
								</div>							
								<div class="form-group">
								    {!! Form::label('slot','Slot') !!}
									{!! Form::text('slot',null,['placeholder'=>'B(10.00-01.00PM)','class'=>'form-control']) !!}
								</div>

								<div class="form-group">
								    {!! Form::label('total_seat','Total seat') !!}
									{!! Form::text('total_seat',null,['placeholder'=>'2011','class'=>'form-control']) !!}
								</div>
							</div>
						</div>
						<div class="form-group">
						{!! Form::submit('Save',['class'=>'marg-top']) !!}
							
						</div>
					</div>
				</div>
			</fieldset>
		{!! Form::close() !!}
   	</div>
@endsection