{{-- Include header section --}}
@include('admin.layouts.include.header')
<!-- Main sidebar -->
<div class="sidebar sidebar-main">
	<div class="sidebar-content">
		@include('admin.layouts.include.userMenu')
		<!-- Main navigation -->
		@include('admin.layouts.include.sidebarMenu')
		<!-- /main navigation -->
	</div>
</div>
			<!-- /main sidebar -->

<!-- Main content -->
<div class="content-wrapper">

	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> 
					@yield('pageTitle')	
				</h4>
			</div>
		</div>
	</div>	
	@yield('content')		
</div>	
<!-- /main content -->
{{-- Include footer section --}}
@include('admin.layouts.include.footer')