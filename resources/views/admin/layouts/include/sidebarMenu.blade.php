<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">

					<!-- Main -->
					<li class="navigation-header"><span>My Menus</span> <i class="icon-menu" title="Main pages"></i></li>
					<li class="@yield('base_menu_dashboard')"><a href="/admin"><i class="icon-home4"></i> <span>Dashboard</span></a></li>							
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>EXAM Info</span></a>
						<ul>
							<li class="@yield('exam_menu_add')">
								<a href="/exam/create">Add new</a>
							</li>
							<li class="@yield('exam_menu_manage')">
								<a href="/exam">Manage Seat plan</a>
							</li>
						</ul>
					</li>					
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>SEAT PLANING</span></a>
						<ul>
							<li class="@yield('awards_menu_add')">
								<a href="/hallseatplan/create">Add new</a>
							</li>
							<li class="@yield('awards_menu_manage')">
								<a href="/hallseatplan">Manage Seat plan</a>
							</li>
						</ul>
					</li>								
						
				</ul>
			</div>
		</div>