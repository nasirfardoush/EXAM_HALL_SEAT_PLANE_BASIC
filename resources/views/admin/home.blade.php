@extends('admin.layouts.master')
@section('educations_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">Hall Seat Plan</span>  || <a href="/hallseatplan/create"> ADD NEW HALL INFO.</a>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
	 <div class="table-responsive">
		<table class="table bg-slate-600">
			<thead>
				<tr>
					<th colspan="10"><h2 class="text-center">
							@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
							@else
								<h4>Master Seat plan</h4>	
							@endif
						</h2>
					</th>
				</tr>
				<tr>
					<th colspan="10">sUMMER SEMESTER 2016</th>
				</tr>
@foreach($alldata as $data)				
				<tr>
					<th colspan="3">Building</th>
					<th colspan="3">Room</th>
					<th colspan="4">Manage</th>
				</tr>					
				<tr>
					<td colspan="3">CSE Building</td>
					<td colspan="3">Room</td>
					<td colspan="4">
						<a class="btn-success" href="/hallseatplan/{{ $data->id }}/edit">Edit</a> 								
						<p class="delbtn">
							{{ Form::open(['url'=>['/hallseatplan',$data->id],'method'=>'DELETE']) }}
								{!! Form::submit('Delete',['class'=>'btn delbtn  btn-danger']) !!}
						  {{ Form::close() }} 
						</p>
						
					</td>
				</tr>				
				<tr>
					<td>Column one</td>					
					<td>Column two</td>					
					<td>Column three</td>					
					<td>Column four</td>					
					<td>Column five</td>					
					<td>Column six</td>					
					<td>Column seven</td>					
					<td>Column eight</td>					
					<td>Column nine</td>					
					<td>Column ten</td>
					
				</tr>				
				<tr>
					<td>{{ $data->col1_text }}</td>					
					<td>{{ $data->col2_text }}</td>					
					<td>{{ $data->col3_text }}</td>					
					<td>{{ $data->col4_text }}</td>					
					<td>{{ $data->col5_text }}</td>					
					<td>{{ $data->col6_text }}</td>					
					<td>{{ $data->col7_text }}</td>					
					<td>{{ $data->col8_text }}</td>					
					<td>{{ $data->col9_text }}</td>					
					<td>{{ $data->col10_text }}</td>
					
				</tr>				
				<tr>
					<td>{{ $data->col1_seat }}</td>					
					<td>{{ $data->col2_seat }}</td>					
					<td>{{ $data->col3_seat }}</td>					
					<td>{{ $data->col4_seat }}</td>					
					<td>{{ $data->col5_seat }}</td>					
					<td>{{ $data->col6_seat }}</td>					
					<td>{{ $data->col7_seat }}</td>					
					<td>{{ $data->col8_seat }}</td>					
					<td>{{ $data->col9_seat }}</td>					
					<td>{{ $data->col10_seat }}</td>
					
				</tr>


@endforeach

			</thead>
			<tbody>
										
			</tbody>
		</table>
	 </div>
 </div>
</div> 	
@endsection