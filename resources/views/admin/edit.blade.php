@extends('admin.layouts.master')
@section('educations_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">HALL SEAT PLAN - EDIT</span>  || <a href="/hallseatplan">HALL SEAT PLAN </a> || <a href="/hallseatplan/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row ">
		    <!-- Update your educations -->
		{!! Form::open(['url'=>['/hallseatplan',$data->id],'method'=>'PUT']) !!}
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
						<div class="row">
						@if(Session::has('message'))
								<div class="alert alert-info " >
									{{ Session::get('message') }}
								</div>
							@else
								<h5>Master Seat Plan .</h5>
							@endif
							<!-- section one -->
							<div class="col-md-5">
								<div class="form-group">									
									{!! Form::label('building','Building Name') !!}
									{!! Form::text('building',$data->building,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>									
								<div class="form-group">
									{!! Form::label('col1_text','First column info.') !!}
									{!! Form::text('col1_text',$data->col1_text,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col2_text','Second column info.') !!}
									{!! Form::text('col2_text',$data->col2_text,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col3_text','Third column info.') !!}
									{!! Form::text('col3_text',$data->col3_text,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col4_text','Fourth column info.') !!}
									{!! Form::text('col4_text',$data->col4_text,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col5_text','Fifth column info.') !!}
									{!! Form::text('col5_text',$data->col5_text,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col6_text','Six column info.') !!}
									{!! Form::text('col6_text',$data->col6_text,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col7_text','Seven no column info.') !!}
									{!! Form::text('col7_text',$data->col7_text,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col8_text','Eight no column info.') !!}
									{!! Form::text('col8_text',$data->col8_text,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col9_text','Nine column info.') !!}
									{!! Form::text('col9_text',$data->col9_text,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col10_text','Ten no column info.') !!}
									{!! Form::text('col10_text',$data->col10_text,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>	
								<div class="form-group">
							     	{!! Form::label('exam_id','Exam Title') !!}

									{!! Form::select('exam_id',
												 [
														"$examData->id" => "$examData->title",
												 ],
										 null,['class' => 'form-control']) !!}	
									
							     	</div>						

							</div>					

							<!-- Second section -->	

							<div class="col-md-5">
								<div class="form-group">
							     	{!! Form::label('room','Exam Room') !!}
									{!! Form::text('room',$data->room,['placeholder'=>'ROOM # 101','class'=>'form-control']) !!}
								</div>					
																<div class="form-group">
									{!! Form::label('col1_seat','Total seat for column no first .') !!}
									{!! Form::text('col1_seat',$data->col1_seat,['placeholder'=>'9','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col2_seat','Total seat for column no second .') !!}
									{!! Form::text('col2_seat',$data->col2_seat,['placeholder'=>'7','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col3_seat','Total seat for column no third .') !!}
									{!! Form::text('col3_seat',$data->col3_seat,['placeholder'=>'7','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col4_seat','Total seat for column no fourth .') !!}
									{!! Form::text('col4_seat',$data->col4_seat,['placeholder'=>'9','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col5_seat','Total seat for column no fifth .') !!}
									{!! Form::text('col5_seat',$data->col5_seat,['placeholder'=>'7','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col6_seat','Total seat for column no six .') !!}
									{!! Form::text('col6_seat',$data->col6_seat,['placeholder'=>'6','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col7_seat','Total seat for Seventh no column info.') !!}
									{!! Form::text('col7_seat',$data->col7_seat,['placeholder'=>'8','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col8_seat','Total seat for Eight no column info.') !!}
									{!! Form::text('col8_seat',$data->col8_seat,['placeholder'=>'7','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col9_seat','Total seat for Nine no column info.') !!}
									{!! Form::text('col9_seat',$data->col9_seat,['placeholder'=>'10','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col10_seat','Total seat for Ten no column.') !!}
									{!! Form::text('col10_seat',$data->col10_seat,['placeholder'=>'7','class'=>'form-control']) !!}
								</div>																
							</div>
						</div>
						<div class="form-group">
						{!! Form::submit('Save',['class'=>'marg-top']) !!}
							
						</div>
					</div>
				</div>
			</fieldset>
		{!! Form::close() !!}
   		 </div>
  </div> 	
@endsection