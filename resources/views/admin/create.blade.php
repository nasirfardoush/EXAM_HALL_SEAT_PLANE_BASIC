@extends('admin.layouts.master')
@section('educations_menu_add','active')
@section('pageTitle')
<span class="text-semibold">MASTER SEAT PLAN - ADD</span>  || <a href="/hallseatplan">MASTER SEAT PLAN</a>
@endsection

@section('content')
	<div class="row ">
		{!! Form::open(['url'=>'/hallseatplan','method'=>'POST']) !!}
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
						<div class="row">
						@if(Session::has('message'))
								<div class="alert alert-info " >
									{{ Session::get('message') }}
								</div>
							@else
								<h5>Master Seat Plan .</h5>
							@endif
							<!-- section one -->
							<div class="col-md-5">
								<div class="form-group">									
									{!! Form::label('building','Building Name') !!}
									{!! Form::text('building',null,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>									
								<div class="form-group">
									{!! Form::label('col1_text','First column info.') !!}
									{!! Form::text('col1_text',null,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col2_text','Second column info.') !!}
									{!! Form::text('col2_text',null,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col3_text','Third column info.') !!}
									{!! Form::text('col3_text',null,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col4_text','Fourth column info.') !!}
									{!! Form::text('col4_text',null,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col5_text','Fifth column info.') !!}
									{!! Form::text('col5_text',null,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col6_text','Six column info.') !!}
									{!! Form::text('col6_text',null,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col7_text','Seven no column info.') !!}
									{!! Form::text('col7_text',null,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col8_text','Eight no column info.') !!}
									{!! Form::text('col8_text',null,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col9_text','Nine column info.') !!}
									{!! Form::text('col9_text',null,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col10_text','Ten no column info.') !!}
									{!! Form::text('col10_text',null,['placeholder'=>'CSE 421,ComputerGraphics,(A-UH)','class'=>'form-control']) !!}
								</div>	
								<div class="form-group">
							     	{!! Form::label('exam_id','Exam Title') !!}

									{!! Form::select('exam_id',
												 [
														"$examData->id" => "$examData->title",
												 ],
										 null,['class' => 'form-control']) !!}	
									
							     	</div>						

							</div>					

							<!-- Second section -->	

							<div class="col-md-5">
								<div class="form-group">
							     	{!! Form::label('room','Exam Room') !!}
									{!! Form::text('room',null,['placeholder'=>'ROOM # 101','class'=>'form-control']) !!}
								</div>					
																<div class="form-group">
									{!! Form::label('col1_seat','Total seat for column no first .') !!}
									{!! Form::text('col1_seat',null,['placeholder'=>'9','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col2_seat','Total seat for column no second .') !!}
									{!! Form::text('col2_seat',null,['placeholder'=>'7','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col3_seat','Total seat for column no third .') !!}
									{!! Form::text('col3_seat',null,['placeholder'=>'7','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col4_seat','Total seat for column no fourth .') !!}
									{!! Form::text('col4_seat',null,['placeholder'=>'9','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col5_seat','Total seat for column no fifth .') !!}
									{!! Form::text('col5_seat',null,['placeholder'=>'7','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col6_seat','Total seat for column no six .') !!}
									{!! Form::text('col6_seat',null,['placeholder'=>'6','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col7_seat','Total seat for Seventh no column info.') !!}
									{!! Form::text('col7_seat',null,['placeholder'=>'8','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col8_seat','Total seat for Eight no column info.') !!}
									{!! Form::text('col8_seat',null,['placeholder'=>'7','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col9_seat','Total seat for Nine no column info.') !!}
									{!! Form::text('col9_seat',null,['placeholder'=>'10','class'=>'form-control']) !!}
								</div>								
								<div class="form-group">
									{!! Form::label('col10_seat','Total seat for Ten no column.') !!}
									{!! Form::text('col10_seat',null,['placeholder'=>'7','class'=>'form-control']) !!}
								</div>																
							</div>
						</div>
						<div class="form-group">
						{!! Form::submit('Save',['class'=>'marg-top']) !!}
							
						</div>
					</div>
				</div>
			</fieldset>
		{!! Form::close() !!}
   	</div>
@endsection