<!DOCTYPE html>
<html lang="en">
<head>
  <title>Exam Hall Seat Plan |DIU</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style >
  	p{
	text-align: center;
	}
	h5{
		text-align: center;
		margin: 2px;

	}
	.main_heading {
	    text-align: center;
	    margin-bottom: 3px;
	    overflow: hidden;
	    position: relative;
	}

	.boder_top{
		border-top: 3px solid #eee;
	}
	table tr td{
		padding: 10px;
		text-align: center;
	}



  </style>
</head>
<body>

	<div class="container">
	   <div class="row">
	   		<div class="maincontent">
	   			<div class="main_heading">
	   		@forelse($examData as $exdata)		
	   				{{ $exdata->title }}
						<div class="main_heading_middle_content">
							<span class="pull-left">Date:  {{$exdata->date}} </span>
	   						<span class="pull-right"> Slot: {{$exdata->slot}} </span>
						</div>
	   				Total Seat: {{$exdata->total_seat}}
	   				<p class="boder_top">MASTER SEAT PLAN</p>
	   			</div>
	   			<div class="content_body">
	@empty
	@endforelse
					{{-- Content Section --}}
				@php
					$colno = 0;
				@endphp
				@forelse($seatPlanData as $data)
					@php
						$colno++;
					@endphp
	   				<section class="row">
	   					<h5><u>CSE Building</u></h5>

	   					<h5>{{ $data->room }} </h5>
		   				<table border="1" cellpadding="0" cellspacing="0">							
								
							
								@if(!empty($data->col1_text))
									<th>
									COL-1 
									</th>
								@endif
															
							
								@if(!empty($data->col2_text))
									<th>
									COL-2 
									</th>
								@endif
															
							
								@if(!empty($data->col3_text))
									<th>
									COL-3 
									</th>
								@endif
															
							
								@if(!empty($data->col4_text))
									<th>
									COL-4 
									</th>
								@endif
															
						
								@if(!empty($data->col5_text))
									<th>
									COL-5 
									</th>
								@endif
														
							
								@if(!empty($data->col6_text))
									<th>
									COL-6 
									</th>
								@endif
														
							
								@if(!empty($data->col7_text))
									<th>
									COL-7 
									</th>
								@endif
														
							
								@if(!empty($data->col8_text))
									<th>
									COL-8 
									</th>
								@endif
														
							
								@if(!empty($data->col9_text))
									<th>
									COL-9 
									</th>
								@endif
																				
							
								@if(!empty($data->col10_text))
									<th>
										COL-10 
									</th>
								@endif
													
							<tbody>
								<tr>
													
									@if(!empty($data->col1_text))
										<td>
											{{ $data->col1_text }}
										</td>
									@endif										
									@if(!empty($data->col2_text))
										<td>
											{{ $data->col2_text }}
										</td>
									@endif										
									@if(!empty($data->col3_text))
										<td>
											{{ $data->col3_text }}
										</td>
									@endif										
									@if(!empty($data->col4_text))
										<td>
											{{ $data->col4_text }}
										</td>
									@endif										
									@if(!empty($data->col5_text))
										<td>
											{{ $data->col5_text }}
										</td>
									@endif										
									@if(!empty($data->col6_text))
										<td>
											{{ $data->col6_text }}
										</td>
									@endif										
									@if(!empty($data->col7_text))
										<td>
											{{ $data->col7_text }}
										</td>
									@endif										
									@if(!empty($data->col8_text))
										<td>
											{{ $data->col8_text }}
										</td>
									@endif										
									@if(!empty($data->col9_text))
										<td>
											{{ $data->col9_text }}
										</td>
									@endif										
									@if(!empty($data->col10_text))
										<td>
											{{ $data->col10_text }}
										</td>
									@endif						
								</tr>							
								<tr>
													
									@if(!empty($data->col1_seat))
										<td>
											{{ $data->col1_seat }}
										</td>
									@endif										
									@if(!empty($data->col2_seat))
										<td>
											{{ $data->col2_seat }}
										</td>
									@endif										
									@if(!empty($data->col3_seat))
										<td>
											{{ $data->col3_seat }}
										</td>
									@endif										
									@if(!empty($data->col4_seat))
										<td>
											{{ $data->col4_seat }}
										</td>
									@endif										
									@if(!empty($data->col5_seat))
										<td>
											{{ $data->col5_seat }}
										</td>
									@endif										
									@if(!empty($data->col6_seat))
										<td>
											{{ $data->col6_seat }}
										</td>
									@endif										
									@if(!empty($data->col7_seat))
										<td>
											{{ $data->col7_seat }}
										</td>
									@endif										
									@if(!empty($data->col8_seat))
										<td>
											{{ $data->col8_seat }}
										</td>
									@endif										
									@if(!empty($data->col9_seat))
										<td>
											{{ $data->col9_seat }}
										</td>
									@endif										
									@if(!empty($data->col10_seat))
										<td>
											{{ $data->col10_seat }}
										</td>
									@endif	
								</tr>
							</tbody>
						</table>
	   				</section>
	   			@empty
	   				<h3>No data Found!!</h3>
	   			@endforelse		

	   			</div>
	   		</div>
	   </div>
	  </div>
</body>
</html>
