@php
$mpdf = new mPDF('s', array(210,297));
 $mpdf->mirrorMargins = 1;
 $mpdf->bleedMargin = 4;

  //Set left to right text
 $mpdf->SetDirectionality('ltr'); 
 $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
 
// $content =  file_get_contents('seatplan.blade.php')
// $mpdf->WriteHTML($content);
         
$mpdf->Output();

exit;