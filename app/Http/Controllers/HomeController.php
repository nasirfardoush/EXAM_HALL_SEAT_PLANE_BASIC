<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SeatPlan;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alldata = SeatPlan::all();
        return view('admin.home',compact('alldata'));
    }
}
