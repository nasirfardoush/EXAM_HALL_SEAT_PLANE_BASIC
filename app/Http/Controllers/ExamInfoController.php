<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExamInfo;
use Session;
use Redirect;
class ExamInfoController extends Controller
{

    public function index()
    {   $examdata = ExamInfo::all();
        return view('admin.exam.index',compact('examdata'));
    }


    public function create()
    {
        return view('admin.exam.create');
    }


    public function store(Request $request)
    {
       ExamInfo::create([
            'title'     =>$request->title,
            'date'      =>$request->date,
            'slot'      =>$request->slot,
            'total_seat'=>$request->total_seat,
        ] );
       Session::flash('message','Successfully added');
       return Redirect::back();
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {   
        $data = ExamInfo::find($id);
        return view('admin.exam.edit',compact('data'));
    }


    public function update(Request $request, $id)
    {
        $update = ExamInfo::find($id);
        $update->title = $request->title;
        $update->date  = $request->date;
        $update->slot  = $request->slot;
        $update->total_seat = $request->total_seat;
        $update->save();

        Session::flash('message','Successfully updated');
        return Redirect::to('/exam');
    }


    public function destroy($id)
    {
         $data = ExamInfo::destroy($id);
         Session::flash('message','Successfully deleted');
         return Redirect::back();
    }
}
