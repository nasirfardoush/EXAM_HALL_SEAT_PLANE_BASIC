<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExamInfo;
use App\SeatPlan;
use Session;
use Redirect;
class HallSeatPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $alldata = SeatPlan::all();
        return view('admin.home',compact('alldata'));
    }


    public function create()
    {   $examData = ExamInfo::first();
        return view('admin.create',compact('examData'));
    }

    public function store(Request $request)
    {
        $seat_plan = new SeatPlan;
        $seat_plan->exam_id = $request->exam_id;
        $seat_plan->building = $request->building;
        $seat_plan->room = $request->room;

        $seat_plan->col1_text = $request->col1_text;
        $seat_plan->col2_text = $request->col2_text;
        $seat_plan->col3_text = $request->col3_text;
        $seat_plan->col4_text = $request->col4_text;
        $seat_plan->col5_text = $request->col5_text;
        $seat_plan->col6_text = $request->col6_text;
        $seat_plan->col7_text = $request->col7_text;
        $seat_plan->col8_text = $request->col8_text;
        $seat_plan->col9_text = $request->col9_text;
        $seat_plan->col10_text = $request->col10_text;  

        $seat_plan->col1_seat = $request->col1_seat;
        $seat_plan->col2_seat = $request->col2_seat;
        $seat_plan->col3_seat = $request->col3_seat;
        $seat_plan->col4_seat = $request->col4_seat;
        $seat_plan->col5_seat = $request->col5_seat;
        $seat_plan->col6_seat = $request->col6_seat;
        $seat_plan->col7_seat = $request->col7_seat;
        $seat_plan->col8_seat = $request->col8_seat;
        $seat_plan->col9_seat = $request->col9_seat;
        $seat_plan->col10_seat = $request->col10_seat;
        $seat_plan->save();
        Session::flash('message','Successfully added');
       return Redirect::back();
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $data = SeatPlan::find($id);
         $examData = ExamInfo::first();
        return view('admin.edit',compact('data','examData'));
    }


    public function update(Request $request, $id)
    {
        $update = SeatPlan::find($id);
        $update->exam_id = $request->exam_id;
        $update->building = $request->building;
        $update->room = $request->room;

        $update->col1_text = $request->col1_text;
        $update->col2_text = $request->col2_text;
        $update->col3_text = $request->col3_text;
        $update->col4_text = $request->col4_text;
        $update->col5_text = $request->col5_text;
        $update->col6_text = $request->col6_text;
        $update->col7_text = $request->col7_text;
        $update->col8_text = $request->col8_text;
        $update->col9_text = $request->col9_text;
        $update->col10_text = $request->col10_text;  

        $update->col1_seat = $request->col1_seat;
        $update->col2_seat = $request->col2_seat;
        $update->col3_seat = $request->col3_seat;
        $update->col4_seat = $request->col4_seat;
        $update->col5_seat = $request->col5_seat;
        $update->col6_seat = $request->col6_seat;
        $update->col7_seat = $request->col7_seat;
        $update->col8_seat = $request->col8_seat;
        $update->col9_seat = $request->col9_seat;
        $update->col10_seat = $request->col10_seat;
        $update->save();
        
        Session::flash('message','Successfully Updated');
       return Redirect::to('/home');
    }


    public function destroy($id)
    {
        $delete = SeatPlan::destroy($id);
        Session::flash('message','Successfully Deleted');
         return Redirect::back();
    }
}
