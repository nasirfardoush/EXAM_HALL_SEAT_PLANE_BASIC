<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExamInfo;
use App\SeatPlan;
use PDF;
class FrontViewController extends Controller
{
    public function index(){
    	$examData = ExamInfo::all();
    	$seatPlanData = SeatPlan::all();
    	return view('home',compact('examData','seatPlanData'));
    }    
    public function seatplan(){
    	$examData = ExamInfo::all();
    	$seatPlanData = SeatPlan::all();
    	return view('seatplan',compact('examData','seatPlanData'));
    }    
   function generate_pdf() {
        $examData = ExamInfo::all();
        $seatPlanData = SeatPlan::all();
    $pdf = PDF::loadView('seatplan', compact('examData','seatPlanData'));
    return $pdf->stream();
}
}
