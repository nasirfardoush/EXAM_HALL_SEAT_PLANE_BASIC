<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamInfo extends Model
{
   protected $table="exam_info";

   protected $fillable=['title','date','slot','total_seat'];



}
