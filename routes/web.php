<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/','FrontViewController@index');
Route::get('/pdf','FrontViewController@generate_pdf');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('hallseatplan','HallSeatPlanController');
Route::resource('exam','ExamInfoController');