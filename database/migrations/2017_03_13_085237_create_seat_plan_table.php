<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seat_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exam_id')->unsigned();
            $table->string('building');
            $table->string('room');
            $table->text('col1_text')->nullable();
            $table->text('col2_text')->nullable();
            $table->text('col3_text')->nullable();
            $table->text('col4_text')->nullable();
            $table->text('col5_text')->nullable();
            $table->text('col6_text')->nullable();
            $table->text('col7_text')->nullable();
            $table->text('col8_text')->nullable();
            $table->text('col9_text')->nullable();
            $table->text('col10_text')->nullable();             
            $table->string('col1_seat')->nullable();
            $table->string('col2_seat')->nullable();
            $table->string('col3_seat')->nullable();
            $table->string('col4_seat')->nullable();
            $table->string('col5_seat')->nullable();
            $table->string('col6_seat')->nullable();
            $table->string('col7_seat')->nullable();
            $table->string('col8_seat')->nullable();
            $table->string('col9_seat')->nullable();
            $table->string('col10_seat')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seat_plan');
    }
}
